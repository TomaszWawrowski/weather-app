import Vue from 'vue';
import Vuex from 'vuex';

import weather from './modules/weather';

Vue.use(Vuex);

export default function () {
  const Store = new Vuex.Store({
    modules: {
      weather,
    },
    strict: process.env.DEV,
  });

  return Store;
}
