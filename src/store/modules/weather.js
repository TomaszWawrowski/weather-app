/* eslint-disable no-shadow */
/* eslint-disable no-empty-pattern */
const axios = require('axios');

const state = {
  data: {},
  responseErrorMessage: '',
  isLoading: false,
  units: 'metric',
};

const mutations = {
  setWeatherData(state, data) {
    state.data = data;
  },
  setLoadingState(state, isLoading) {
    state.isLoading = isLoading;
  },
  setResponseErrorMsg(state, message) {
    state.responseErrorMessage = message;
  },
};

const actions = {
  getCurrentWeather({ commit, state }, params) {
    commit('setResponseErrorMsg', '');
    axios
      .get('https://api.openweathermap.org/data/2.5/weather', {
        params: {
          ...params,
          appid: '027ee07fafd9a678d925c3a9220c1289',
          units: state.units,
        },
      })
      .then((response) => {
        commit('setWeatherData', response.data);
        commit('setLoadingState', false);
      })
      .catch((error) => {
        commit('setResponseErrorMsg', error.response.data.message);
      });
  },
};

const getters = {
  getWeatherCalculationTime: (state) => {
    const unixTS = state.data.dt;
    return unixTS ? new Date(unixTS * 1000).toLocaleDateString() : '';
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
